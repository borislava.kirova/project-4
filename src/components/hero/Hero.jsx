import Typography from "@mui/material/Typography";

import styles from "./Hero.module.scss";

export default function Hero({ text }) {
  return (
    <div className={styles["hero"]}>
      <Typography className={styles["text"]} variant="h1" gutterBottom>
        {text}
      </Typography>
    </div>
  );
}
