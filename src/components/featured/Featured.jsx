import Container from "@mui/material/Container";
import ImageList from "@mui/material/ImageList";
import ImageListItem from "@mui/material/ImageListItem";
import { useRouter } from "next/router";
import styles from "./Featured.module.scss";
import classNames from "classnames";

/**
 * @param {Array<{ image: String, title: String, rows: Number, cols: Number, href: String }>} items
 */
export default function Featured({ items = [] }) {
  const router = useRouter();
  const handleClick = (href) => {
    router.push(href);
  };
  console.log(items);
  return (
    <Container className={classNames(styles.container)} maxWidth="xl">
      <ImageList
        variant="quilted"
        cols={6}
        rowHeight={226}
        gap={20}
        className={classNames(styles.list)}
      >
        {items.map((item, i) => (
          <ImageListItem
            key={i}
            cols={item.cols || 1} // doesn't exist in API
            rows={item.rows || 1}
            className={classNames(styles.item)}
            onClick={() => handleClick()}
          >
            <img src={item.image} alt={item.name} loading="lazy" />
          </ImageListItem>
        ))}
      </ImageList>
    </Container>
  );
}
