import { Button, Container, Grid, Stack } from "@mui/material";
import Logo from "../logo/Logo";
import styles from "./Footer.module.scss";
import classNames from "classnames";

export default function Footer() {
  return (
    <div className={classNames(styles.wrapper)}>
      <Container className={classNames(styles.container)} maxWidth="xl">
        <Grid
          container
          spacing={2}
          className={styles.footer}
          alignItems="center"
          justifyContent="center"
        >
          <Grid item xs={3}>
            <Logo type="muted" />
          </Grid>
          <Grid item xs={4}>
            <p className={styles.rights}>Bum All Rights Reserved 2021</p>
          </Grid>
          <Grid item xs={4}>
            <Stack spacing={0.5} direction="row">
              <Button className={styles.button} variant="link" disableRipple>
                Privacy Policy
              </Button>
              <Button className={styles.button} variant="link" disableRipple>
                Cookie Policy
              </Button>
            </Stack>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}
