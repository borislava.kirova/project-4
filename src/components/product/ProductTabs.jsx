import styles from "./ProductTabs.module.scss";
import classNames from "classnames";
import { formatDistance, parseISO } from "date-fns";

import * as React from "react";
import TableContainer from "@mui/material/TableContainer";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import User from "../user/User";

export default function ProductTabs({ text, bids }) {
  const [value, setValue] = React.useState("1");

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={styles["product-tabs"]}>
      <TabContext value={value}>
        <Box className={styles["tab-box"]}>
          <TabList onChange={handleChange}>
            <Tab
              className={(styles["tab"], "tab-details")}
              label="Details"
              value="1"
            />
            <Tab
              className={(styles["tab"], "tab-bids")}
              label="Bids"
              value="2"
            />
          </TabList>
        </Box>
        <TabPanel
          className={classNames(
            styles["tab-panel"],
            styles["tab-panel-description"]
          )}
          value="1"
        >
          <Typography variant="body2" gutterBottom>
            {text}
          </Typography>
        </TabPanel>
        <TabPanel className={styles["tab-panel"]} value="2">
          <TableContainer>
            <Table className={styles["table"]}>
              <TableBody>
                {bids.map((bid, i) => (
                  <TableRow
                    key={i}
                    className={(styles["table-row"], `table-row-${i}`)}
                  >
                    <TableCell
                      className={styles["table-cell"]}
                      component="th"
                      scope="row"
                    >
                      <User {...bid.user} size={30} />
                    </TableCell>
                    <TableCell className={styles["table-cell"]} align="right">
                      <span className={styles["price"]}>{bid.amount} ETH</span>
                    </TableCell>
                    <TableCell className={styles["table-cell"]} align="right">
                      {formatDistance(parseISO(bid.date), Date.now(), {
                        addSuffix: true,
                      })}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </TabPanel>
      </TabContext>
    </div>
  );
}
