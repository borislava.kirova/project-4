import styles from "./ProductInfoLikes.module.scss";
import { Chip } from "@mui/material";
import millify from "millify";
import { Favorite as FavoriteIcon } from "@mui/icons-material";

export default function ProductInfoLikes({ amount = 0 }) {
  return (
    <div className={styles["product-info-likes"]}>
      <Chip
        className={styles.likes}
        clickable
        icon={<FavoriteIcon className={styles.icon} />}
        label={millify(amount)}
        color="success"
        variant="outlined"
      />
    </div>
  );
}
