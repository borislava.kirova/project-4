import styles from "./ProductContainer.module.scss";

import ProductImage from "./ProductImage";
import ProductInfo from "./ProductInfo.jsx";
import ProductTabs from "./ProductTabs.jsx";
import ProductActions from "./ProductActions.jsx";

import { Grid } from "@mui/material";

export default function ProductContainer({
  name,
  owner,
  price,
  currency,
  likes,
  auction_end,
  details,
  bids,
  source,
}) {
  return (
    <div className={styles["product-container"]}>
      <Grid container justifyContent="space-between">
        <Grid item xs={6}>
          <ProductImage url={source.url} />
        </Grid>
        <Grid item xs={5}>
          <ProductInfo
            title={name}
            creator={{
              name: owner.username,
              avatar: owner.avatar.url,
              verified: owner.verified,
            }}
            price={price}
            currency={currency}
            likes={likes}
            timeEnd={auction_end}
            isLive={auction_end}
            onTimeEnd={() => {
              console.log("time end");
            }}
          />
          <ProductTabs text={details} bids={bids} />
          <ProductActions
            isLive={auction_end}
            currency={currency}
            buyAmount={3}
            bidAmount={1}
            onBid={() => {
              console.log("bid");
            }}
            onBuy={() => {
              console.log("buy");
            }}
          />
        </Grid>
      </Grid>
    </div>
  );
}
