import styles from "./ProductInfoTimer.module.scss";
import classNames from "classnames";
import Countdown from "react-countdown";

export default function ProductInfoTimer({ timeEnd, onTimeEnd }) {
  return (
    <div
      className={classNames(styles["product-info-timer"], {
        [styles["active"]]: timeEnd,
      })}
    >
      <div className={styles["title"]}>Ends in</div>

      <div className={styles["timer"]}>
        {timeEnd && <Countdown date={timeEnd} onStop={onTimeEnd}></Countdown>}
      </div>
    </div>
  );
}
