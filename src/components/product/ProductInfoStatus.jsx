import styles from "./ProductInfoStatus.module.scss";
import { Chip } from "@mui/material";
import FiberManualRecordIcon from "@mui/icons-material/FiberManualRecord";

export default function ProductInfoStatus() {
  return (
    <div className={styles["product-info-status"]}>
      <Chip
        className={styles.status}
        icon={<FiberManualRecordIcon className={styles.icon} />}
        label={"LIVE"}
        color="success"
      />
    </div>
  );
}
