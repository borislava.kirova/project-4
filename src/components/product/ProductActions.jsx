import { Button, Grid } from "@mui/material";

import styles from "./ProductActions.module.scss";

export default function ProductActions({
  isLive,
  currency,
  buyAmount,
  bidAmount,
  onBuy,
  onBid,
}) {
  return (
    <div className={styles["product-actions"]}>
      <Grid
        container
        justifyContent="space-between"
        alignItems="center"
        mt={2}
        spacing={1}
      >
        <Grid item xs={7}>
          <Button
            className={styles.button}
            variant="contained"
            onClick={onBuy}
            disabled={Boolean(!isLive)}
          >
            Buy for {buyAmount} {currency}
          </Button>
        </Grid>
        <Grid item xs={5}>
          <Button
            className={styles.button}
            color="success"
            variant="outlined"
            onClick={onBid}
            disabled={Boolean(!isLive)}
          >
            Place bid for {bidAmount} {currency}
          </Button>
        </Grid>
      </Grid>
    </div>
  );
}
