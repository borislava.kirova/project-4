import styles from "./ProductInfo.module.scss";

import ProductInfoTitle from "./ProductInfoTitle.jsx";
import ProductInfoPrice from "./ProductInfoPrice.jsx";
import ProductInfoStatus from "./ProductInfoStatus.jsx";
import ProductInfoLikes from "./ProductInfoLikes.jsx";
import ProductInfoCreator from "./ProductInfoCreator.jsx";
import ProductInfoTimer from "./ProductInfoTimer.jsx";

import { Stack, Grid } from "@mui/material";

export default function ProductInfo({
  title,
  creator,
  price,
  currency,
  likes,
  onTimeEnd,
  timeEnd,
  isLive,
}) {
  return (
    <div className={styles["product-info"]}>
      <ProductInfoTitle text={title} />

      <Stack
        className={styles["stats"]}
        direction="row"
        justifyContent="space-between"
        alignItems="center"
      >
        <ProductInfoPrice amount={price} currency={currency} />
        <Stack spacing={0.5} direction="row">
          {isLive && <ProductInfoStatus />}
          <ProductInfoLikes amount={likes} />
        </Stack>
      </Stack>
      <Grid
        container
        justifyContent="space-between"
        alignItems="stretch"
        spacing={2}
      >
        <Grid item xs={7}>
          <ProductInfoCreator {...creator} />
        </Grid>
        <Grid item xs={5}>
          <ProductInfoTimer onTimeEnd={onTimeEnd} timeEnd={timeEnd} />
        </Grid>
      </Grid>
    </div>
  );
}
