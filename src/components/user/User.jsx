import Avatar from "../avatar/Avatar";
import styles from "./User.module.scss";

export default function User({
  name = "",
  info = "",
  avatar = "",
  verified = false,
  size = 55,
}) {
  return (
    <div className={styles.user}>
      <Avatar url={avatar} size={size} verified={verified} />
      <div className={styles.details}>
        <span className={styles.name}>{name}</span>
        <span className={styles.info}>{info}</span>
      </div>
    </div>
  );
}
