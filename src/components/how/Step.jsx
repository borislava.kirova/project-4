import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import styles from "./Step.module.scss";
import classNames from "classnames";

/**
 * @param {String} title
 * @param {String} description
 * @param {Number} number
 */
export default function Step({ title, description, number }) {
  return (
    <Paper className={classNames(styles.container)}>
      <Grid
        container
        spacing={2}
        justifyContent="space-between"
        className={classNames(styles.grid)}
      >
        <Grid item xs={4} className={classNames(styles.left)}>
          <div className={classNames(styles.number)}>{number}</div>
        </Grid>
        <Grid item xs={8} className={classNames(styles.right)}>
          <Typography
            className={classNames(styles.title)}
            variant="h5"
            gutterBottom
          >
            {title}
          </Typography>
          <Typography
            className={classNames(styles.description)}
            variant="body2"
            gutterBottom
          >
            {description}
          </Typography>
        </Grid>
      </Grid>
    </Paper>
  );
}
