import Container from "@mui/material/Container";
import Button from "@mui/material/Button";
import Link from "@mui/material/Link";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import styles from "./How.module.scss";
import classNames from "classnames";
import Step from "./Step";

/**
 * @param {String} title
 * @param {String} description
 * @param {String} link
 * @param {Array<{ title: String, description: String }>} items
 */
export default function How({ title, description, items = [], link }) {
  return (
    <div className={classNames(styles.wrapper)}>
      <Container className={classNames(styles.container)} maxWidth="xl">
        <Grid
          container
          spacing={2}
          justifyContent="space-between"
          alignItems="center"
        >
          <Grid item xs={5} className={classNames(styles.left)}>
            <Typography
              className={classNames(styles.title)}
              variant="h1"
              gutterBottom
            >
              {title}
            </Typography>
            <Typography
              className={classNames(styles.description)}
              variant="body1"
              gutterBottom
            >
              {description}
            </Typography>

            <Button
              className={classNames(styles.button)}
              variant="contained"
              component={Link}
              href={link}
            >
              Learn more
            </Button>
          </Grid>
          <Grid item xs={5} className={classNames(styles.right)}>
            {items.map((item, i) => (
              <Step {...item} number={i + 1} key={i} />
            ))}
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}
