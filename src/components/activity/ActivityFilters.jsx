import styles from "./ActivityFilters.module.scss";

import {
  FormControl,
  Select,
  InputLabel,
  MenuItem,
  Stack,
  TextField,
  InputAdornment,
} from "@mui/material";
import { Search } from "@mui/icons-material";

export default function ActivityFilters({ filters }) {
  return (
    <div className={styles["activity-filters"]}>
      <Stack spacing={0.5} direction="row">
        <FormControl>
          <InputLabel htmlFor="select">Sort by</InputLabel>
          <Select
            className={styles["form-control-select"]}
            inputProps={{
              id: "select",
            }}
            color="primary"
            label="This week"
            variant="outlined"
          >
            {filters.sort.map((item, i) => (
              <MenuItem value={item.value} key={i}>
                {item.label}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="select">Type</InputLabel>
          <Select
            className={styles["form-control-select"]}
            inputProps={{
              id: "select",
            }}
            color="primary"
            label="This week"
            variant="outlined"
          >
            {filters.type.map((item, i) => (
              <MenuItem value={item.value} key={i}>
                {item.label}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <div className={styles.search}>
          <TextField
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  {" "}
                  <Search color="primary" />
                </InputAdornment>
              ),
              disableUnderline: true,
            }}
            variant="standard"
          />
        </div>
      </Stack>
    </div>
  );
}
