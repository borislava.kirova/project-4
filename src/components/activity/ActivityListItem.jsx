import Avatar from "../avatar/Avatar";
import Link from "../link/Link";
import styles from "./ActivityListItem.module.scss";
import { formatDistance, parseISO } from "date-fns";

export default function ActivityListItem({
  user,
  created_at = Date,
  nft,
  type = "like",
}) {
  switch (type) {
    case "buy":
      type = "bought";
      break;

    case "like":
      type = "liked";
      break;

    default:
      type = "no data";
      break;
  }

  return (
    <div className={styles.wrapper}>
      <div className={styles.avatar}>
        <Avatar url={user.avatar.url} size={50} verified={user.verified} />
      </div>
      <div className={styles.content}>
        <p className={styles.liked}>
          {user.name} {type}{" "}
          <Link href="/" color="secondary">
            “{nft.name}”
          </Link>{" "}
          by{" "}
          <Link href="/" color="secondary">
            {nft.owner.username}
          </Link>
        </p>
        {formatDistance(parseISO(created_at), Date.now(), {
          addSuffix: true,
        })}
      </div>
    </div>
  );
}
