import styles from "./ActivityList.module.scss";
import ActivityListItem from "./ActivityListItem";
import { Stack } from "@mui/material";
export default function ActivityList({ items }) {
  console.log(items);
  return (
    <div className={styles["activity-list"]}>
      <Stack spacing={1} direction="column">
        {items.map((item, i) => (
          <ActivityListItem key={i} {...item} />
        ))}
      </Stack>
    </div>
  );
}
