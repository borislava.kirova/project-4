import CollectorColumn from "./CollectorColumn";
import {
  Container,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import { chunk } from "lodash";
import styles from "./TopCollectors.module.scss";

export default function TopCollectors({ collectors = [] }) {
  collectors.map((collectorInfo, i) =>
    Object.assign(collectorInfo, { displayId: i + 1 })
  );
  const organizedCollectors = chunk(collectors, 3);

  return (
    <div>
      <Container maxWidth="xl">
        <div className={styles["heading-wrapper"]}>
          <h2 className={styles.heading}>Top Collectors</h2>
          <FormControl>
            <InputLabel htmlFor="select">This week</InputLabel>
            <Select
              className={styles["form-control-select"]}
              inputProps={{
                id: "select",
              }}
              color="primary"
              label="This week"
              variant="outlined"
            >
              <MenuItem>Something</MenuItem>
            </Select>
          </FormControl>
        </div>
        <Grid container spacing={3}>
          {organizedCollectors.map((collectorColumn, i) => (
            <Grid item key={i} xs={3}>
              <CollectorColumn items={collectorColumn} />
            </Grid>
          ))}
        </Grid>
      </Container>
    </div>
  );
}
