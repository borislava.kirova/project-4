import User from "../user/User";
import styles from "./Collector.module.scss";

export default function Collector({
  id,
  name = "Pesho",
  nftsCount,
  avatar,
  type,
  verified,
}) {
  return (
    <div className={styles.container}>
      <div className={`${styles.index} ${type}`}>
        <p>{id}</p>
      </div>
      <div className={styles.content}>
        <User
          name={name}
          avatar={avatar}
          verified={verified}
          info={`${nftsCount} items`}
        />
      </div>
    </div>
  );
}
