import Collector from "./Collector";
import styles from "./CollectorColumn.module.scss";

export default function CollectorColumn({ items = [] }) {
  return (
    <div className={styles["column-wrapper"]}>
      {items.map((collector, i) => (
        <Collector
          key={i}
          id={collector.displayId}
          name={collector.username}
          nftsCount={collector.nftsCount}
          avatar={collector.avatar.url}
          verified={collector.confirmed}
          type={i === 2 ? "light" : ""}
        />
      ))}
    </div>
  );
}
