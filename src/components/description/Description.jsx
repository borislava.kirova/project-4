import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";

import styles from "./Description.module.scss";

export default function Description({ text, image }) {
  return (
    <div className={styles["description"]}>
      <Container className={styles["container"]} maxWidth="md">
        <Typography className={styles["text"]} variant="body1" gutterBottom>
          {text}
        </Typography>

        <div className={styles["image-container"]}>
          <img src={image} className={styles["image"]} />
        </div>
      </Container>
    </div>
  );
}
