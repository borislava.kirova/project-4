import styles from "./ProfileCollection.module.scss";

import ProfileCollectionFilters from "./ProfileCollectionFilters";
import Card from "../card/Card";
import { Container, Grid, Typography } from "@mui/material";

export default function ProfileCollection({ user, items, filters }) {
  return (
    <div className={styles["profile-collection"]}>
      <Container>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item xs={3}>
            <Typography className={styles["title"]} variant="h3">
              Collection
            </Typography>
          </Grid>
          <Grid item xs={9}>
            <ProfileCollectionFilters filters={filters} />
          </Grid>
        </Grid>
        <Grid
          container
          justifyContent="space-between"
          mt={3}
          mb={3}
          spacing={1}
        >
          {items.map((item, i) => (
            <Grid item xs={3} key={i}>
              <Card
                name={item.name}
                likes={item.likes}
                user={user}
                mediaUrl={item.source.url}
                price={item.price}
                currency={item.currency}
                timeLeft={item.auction_end}
              />
            </Grid>
          ))}
        </Grid>
      </Container>
    </div>
  );
}
