import styles from "./ProfileCollectionFilters.module.scss";

import {
  FormControl,
  Select,
  InputLabel,
  MenuItem,
  Stack,
  TextField,
  InputAdornment,
} from "@mui/material";
import { Search } from "@mui/icons-material";

export default function ProfileCollectionFilters({ filters }) {
  return (
    <div className={styles["profile-collection-filters"]}>
      <Stack spacing={0.5} direction="row">
        <FormControl>
          <InputLabel htmlFor="select">Sort by</InputLabel>
          <Select
            className={styles["form-control-select"]}
            inputProps={{
              id: "select",
            }}
            color="primary"
            label="This week"
            variant="outlined"
          >
            {filters.sort.map((item, i) => (
              <MenuItem value={item.value}>{item.label}</MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl>
          <InputLabel htmlFor="select">Price range</InputLabel>
          <Select
            className={styles["form-control-select"]}
            inputProps={{
              id: "select",
            }}
            color="primary"
            label="This week"
            variant="outlined"
          >
            {filters.price.map((item, i) => (
              <MenuItem value={item.value}>{item.label}</MenuItem>
            ))}
          </Select>
        </FormControl>
        <div className={styles.search}>
          <TextField
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  {" "}
                  <Search color="primary" />
                </InputAdornment>
              ),
              disableUnderline: true,
            }}
            variant="standard"
          />
        </div>
      </Stack>
    </div>
  );
}
