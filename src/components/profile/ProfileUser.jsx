import styles from "./ProfileUser.module.scss";

import Avatar from "../avatar/Avatar";
import { Stack, Typography } from "@mui/material";

export default function ProfileUser({ name, info, avatar, verified }) {
  return (
    <div className={styles["profile-user"]}>
      <Stack
        spacing={1}
        direction="column"
        justifyContent="center"
        alignItems="center"
      >
        <Avatar url={avatar} size={150} verified={verified} />
        <Typography variant="h3">{name}</Typography>
        <Typography variant="body1">{info}</Typography>
      </Stack>
    </div>
  );
}
