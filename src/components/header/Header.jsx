import { Button, Container, Grid, InputBase, Stack } from "@mui/material";
import Logo from "../logo/Logo";
import { Search } from "@mui/icons-material";
import { styled } from "@mui/system";
import styles from "./Header.module.scss";
import classNames from "classnames";

const Input = styled(InputBase)`
  &.MuiInputBase-root {
    flex: 1;
  }
`;

export default function Header() {
  return (
    <div className={classNames(styles.wrapper)}>
      <Container className={classNames(styles.container)} maxWidth="xl">
        <Grid
          container
          spacing={2}
          className={styles.header}
          alignItems="center"
          justifyContent="center"
        >
          <Grid item xs={2}>
            <Logo />
          </Grid>
          <Grid item xs={6}>
            <div className={styles.search}>
              <Search />
              <Input placeholder="Find items, users and activities" />
            </div>
          </Grid>
          <Grid item xs={4}>
            <Stack spacing={0.5} direction="row">
              <Button className={styles.button} variant="link" disableRipple>
                Home
              </Button>
              <Button className={styles.button} variant="link" disableRipple>
                Activity
              </Button>
              <Button className={styles.button} variant="contained">
                Explore
              </Button>
            </Stack>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}
