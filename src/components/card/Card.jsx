import { Chip, Typography } from "@mui/material";
import {
  Favorite as FavoriteIcon,
  FiberManualRecord as LiveIcon,
} from "@mui/icons-material";

import Avatar from "../avatar/Avatar";
import CardHeader from "@mui/material/CardHeader";
import CardMedia from "@mui/material/CardMedia";
import MuiCard from "@mui/material/Card";
import clsx from "clsx";
import millify from "millify";
import styles from "./Card.module.scss";
import Countdown from "react-countdown";

export default function Card({
  name = "",
  likes = 0,
  user = {},
  mediaUrl = "",
  price = 0,
  currency = "",
  timeLeft,
}) {
  return (
    <MuiCard
      className={clsx(styles.card, {
        [styles.live]: Boolean(timeLeft),
      })}
      sx={{ maxWidth: 345 }}
    >
      <CardHeader
        avatar={
          <Avatar
            url={user?.avatar?.url}
            size={35}
            verified={user?.confirmed}
          />
        }
      />
      <div className={styles.mediaContainer}>
        <CardMedia className={styles.media} component="img" image={mediaUrl} />
        <Chip
          icon={<LiveIcon className={styles.icon} />}
          className={styles.badge}
          label="Live"
        />
        <Countdown date={timeLeft} className={styles.timer}></Countdown>
      </div>
      <div className={styles.footer}>
        <div>
          <Typography className={styles.title} variant="body1">
            {name}
          </Typography>
          <Typography className={styles.price} variant="body1">
            ~{price} {currency}
          </Typography>
        </div>
        <Chip
          className={styles.likes}
          clickable
          icon={<FavoriteIcon className={styles.icon} />}
          label={millify(likes)}
          color="success"
          variant="outlined"
        />
      </div>
    </MuiCard>
  );
}
