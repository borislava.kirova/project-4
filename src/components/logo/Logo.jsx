export default function Logo({ type = "default" }) {
  return (
    <div>
      {type === "muted" ? (
        <img src="/images/logo-muted.svg" alt="" />
      ) : (
        <img src="/images/logo.svg" alt="" />
      )}
    </div>
  );
}
