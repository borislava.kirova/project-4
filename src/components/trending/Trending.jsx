import {
  Container,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import Card from "../card/Card";
import styles from "./Trending.module.scss";

/**
 * @param {Array<{ name: String, user: String, mediaUrl: String, price: Number, currency: String }>} cards
 */
export default function Trending({ cards = [] }) {
  return (
    <div>
      <Container maxWidth="xl">
        <div className={styles["heading-wrapper"]}>
          <h2 className={styles.heading}>Trending</h2>
          <FormControl>
            <InputLabel htmlFor="select">This week</InputLabel>
            <Select
              className={styles["form-control-select"]}
              inputProps={{
                id: "select",
              }}
              color="primary"
              label="This week"
              variant="outlined"
            >
              <MenuItem>Something</MenuItem>
            </Select>
          </FormControl>
        </div>
        <Grid
          maxWidth="xl"
          container
          spacing={3}
          alignItems="center"
          justifyContent="center"
        >
          {cards.map((card, i) => (
            <Grid item key={i}>
              <Card
                name={card.name}
                user={card.owner}
                mediaUrl={card.mediaUrl}
                price={card.price}
                currency={card.currency}
                likes={card.likes}
              />
            </Grid>
          ))}
        </Grid>
      </Container>
    </div>
  );
}
