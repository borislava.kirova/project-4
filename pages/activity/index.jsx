import Header from "../../src/components/header/Header";
import Footer from "../../src/components/footer/Footer";
import Hero from "../../src/components/hero/Hero";
import ActivityList from "../../src/components/activity/ActivityList";
import ActivityFilters from "../../src/components/activity/ActivityFilters";
import activityData from "../../data/activity.json";
import styles from "./activity.module.scss";
import filtersData from "../../data/filtersActivity.json";

export default function Index() {
  return (
    <div>
      <Header />
      <Hero text={"Activity"} />
      <div className={styles["wrapper"]}>
        <ActivityFilters filters={filtersData} />
        <ActivityList items={activityData} />
      </div>
      <Footer />
    </div>
  );
}
