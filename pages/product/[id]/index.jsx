import Header from "../../../src/components/header/Header";
import Footer from "../../../src/components/footer/Footer";
import ProductContainer from "../../../src/components/product/ProductContainer";

import dataProduct from "../../../data/nfts.json";

import { useRouter } from "next/router";

export default function Index() {
  const router = useRouter();
  const { id } = router.query;
  const product =
    dataProduct.find((i) => String(i.id) === id) || dataProduct[0];

  return (
    <div>
      <Header />
      <ProductContainer {...product} />
      <Footer />
    </div>
  );
}
