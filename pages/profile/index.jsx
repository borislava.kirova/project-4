import Header from "../../src/components/header/Header";
import Footer from "../../src/components/footer/Footer";
import ProfileHero from "../../src/components/profile/ProfileHero";
import ProfileUser from "../../src/components/profile/ProfileUser";
import ProfileCollection from "../../src/components/profile/ProfileCollection";
import profileData from "../../data/profile.json";
import filtersData from "../../data/filtersProfile.json";

export default function Index() {
  return (
    <div>
      <Header />
      <ProfileHero image={profileData.avatar.backgroundUrl} />
      <ProfileUser
        name={profileData.username}
        avatar={profileData.avatar.url}
        info={profileData.info}
        verified={profileData.verified}
      />
      <ProfileCollection
        user={profileData}
        items={profileData.nfts}
        filters={filtersData}
      />
      <Footer />
    </div>
  );
}
