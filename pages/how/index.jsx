import Header from "../../src/components/header/Header";
import Footer from "../../src/components/footer/Footer";
import Hero from "../../src/components/hero/Hero";
import Description from "../../src/components/description/Description";

import styles from "./how.module.scss";

let text =
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur posuere bibendum eros et pharetra. Aenean sodales odio quis augue imperdiet, id mollis urna laoreet. Curabitur consequat arcu nec tortor suscipit, ornare pulvinar tellus convallis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris lacus urna, interdum non purus posuere, aliquet feugiat mi. Maecenas ultricies tristique arcu, eget consectetur tortor laoreet eget. Aliquam venenatis, elit a tincidunt maximus, magna justo pulvinar felis, ac sollicitudin nibh neque sed turpis. Fusce tellus est, dignissim id blandit ut, fringilla ut urna. Donec imperdiet, quam nec consectetur fringilla, lorem nisl gravida tellus, eu congue lorem erat nec nisl. Quisque quis porta ex, eu porttitor nisi. Vestibulum at mauris odio. Nullam aliquam, turpis vel convallis ullamcorper, tortor diam iaculis turpis, quis eleifend turpis neque sit amet odio. Sed et varius tortor, id mollis lorem. Suspendisse potenti. Nunc facilisis rutrum quam nec auctor.";
let image =
  "https://nft-auction.herokuapp.com/uploads/0x72abed3186b65b29e4da3faaa926e74d1f763cc5_c3e6697515.jpg";

export default function Index() {
  return (
    <div>
      <Header />
      <Hero text={"How It Works"} />
      <div className={styles.description}>
        <Description text={text} image={image} />
      </div>
      <Footer />
    </div>
  );
}
